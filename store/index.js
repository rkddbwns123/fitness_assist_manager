import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import menu from './modules/menu'
import testData from './modules/test-data'
import fitnessAssist from './modules/fitness-assist'
import userStat from './modules/user-stat'

export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    menu,
    testData,
    fitnessAssist,
    userStat
}
