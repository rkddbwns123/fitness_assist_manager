export default {
    DO_CREATE_INFO: 'fitness-assist/doCreateInfo',
    DO_LIST: 'fitness-assist/doList',
    DO_UPDATE: 'fitness-assist/doUpdate',
    DO_DETAIL: 'fitness-assist/doDetail',
}
