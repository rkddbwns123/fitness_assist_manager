const BASE_URL = '/v1/user-info'

export default {
    DO_CREATE_INFO: `${BASE_URL}/data`, //post
    DO_LIST: `${BASE_URL}/all`, //get
    DO_UPDATE: `${BASE_URL}/update/id/{id}`, //put
    DO_DETAIL: `${BASE_URL}/detail/{id}`, //get
}
